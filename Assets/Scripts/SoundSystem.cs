﻿using System;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Sounds
{
    public String name;
    public AudioClip clip;

    public static implicit operator AudioClip(Sounds v)
    {
        throw new NotImplementedException();
    }
}

[System.Serializable]
public class SoundSystem
{
    public AudioSource source;
    public List<Sounds> sounds;

    public void PlaySound(String name)
    {
        AudioClip clip = null;
        foreach(Sounds sound in sounds)
        {
            if(sound.name == name)
            {
                clip = sound.clip;
            }
        }
        source.clip = clip;
        source.Play();
    }

    public void PlaySound(int index)
    {
        source.clip = sounds[index].clip;
        source.Play();
    }
}
