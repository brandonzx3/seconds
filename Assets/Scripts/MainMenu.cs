using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenu : MonoBehaviour
{
    [SerializeField] private LevelLoader loader;
    [SerializeField] private MenuManager mm;


    public void Start()
    {
        loader = GameObject.FindGameObjectWithTag("GameController").GetComponent<LevelLoader>();
        mm = GameObject.FindGameObjectWithTag("GameController").GetComponent<MenuManager>();
    }

    public void PlayGame()
    {
        loader.LoadScene(1);
    }

    public void OpenSettings()
    {
        mm.OpenSettingsGui();
    }

    public void CloseGame()
    {
        mm.CloseGame();
    }
}
