using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadingZoneController : MonoBehaviour
{

    private GameObject player;
    private LevelLoader loader;
    private GameObject cam;
    [SerializeField] private bool switchScene;
    [SerializeField] private bool needsInteract;
    [SerializeField] private Vector2 loadPos;
    [SerializeField] private string scene;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        loader = GameObject.FindGameObjectWithTag("GameController").GetComponent<LevelLoader>();
        cam = GameObject.FindGameObjectWithTag("MainCamera");
    }

    // Update is called once per frame
    void Update()
    {
        if(needsInteract)
        {
            if(Vector3.Distance(gameObject.transform.position, player.transform.position) <= 1)
            {
                if(Input.GetKeyDown(KeyCode.E))
                {
                    MovePlayer();
                }
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(!needsInteract && collision.gameObject.tag == "Player")
        {
            MovePlayer();
        }
    }

    private void MovePlayer()
    {
        if (switchScene)
        {
            loader.LoadScene(scene);
        }
        player.transform.position = loadPos;
        cam.transform.position = new Vector3(loadPos.x, loadPos.y, -1);
    }
}
