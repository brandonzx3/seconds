using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuManager : MonoBehaviour
{
    private GameManager gm;
    private LevelLoader loader;
    private Settings settings;
    private bool isPaused = false;
    [SerializeField] private GameObject pauseGui;
    [SerializeField] private GameObject leaveLevelGui;
    [SerializeField] private GameObject settingsGui;
    [SerializeField] private GameObject[] buttons;

    private void Start()
    {
        gm = GetComponent<GameManager>();
        loader = GetComponent<LevelLoader>();
        settings = GetComponent<Settings>();
        pauseGui.SetActive(false);
        leaveLevelGui.SetActive(false);
        settingsGui.SetActive(false);
    }

    public void Update()
    {
        if(SceneManager.GetActiveScene().buildIndex != 0 && Input.GetKeyDown(KeyCode.Escape))
        {
            Pause();
        }
    }

    public void Pause()
    {
        isPaused = !isPaused;
        if(isPaused)
        {
            pauseGui.SetActive(true);
            Time.timeScale = 0f;
        } else
        {
            CloseLeaveLevelGui();
            CloseSettingsGui();
            pauseGui.SetActive(false);
            Time.timeScale = 1f;
        }
    }


    private void ToggleDefaultButtons(bool toggle)
    {
        foreach (GameObject button in buttons)
        {
            button.GetComponent<Button>().interactable = toggle;
        }
    }

    public void OpenExitLevelWarning()
    {
        ToggleDefaultButtons(false);
        leaveLevelGui.SetActive(true);
    }

    public void CloseLeaveLevelGui()
    {
        ToggleDefaultButtons(true);
        leaveLevelGui.SetActive(false);
    }

    public void LoadMainMenu()
    {
        loader.LoadScene(0);
        isPaused = false;
        pauseGui.SetActive(false);
        CloseLeaveLevelGui();
    }

    public void OpenSettingsGui()
    {
        settings.LoadSettings();
        ToggleDefaultButtons(false);
        settingsGui.SetActive(true);
    }

    public void CloseSettingsGui()
    {
        ToggleDefaultButtons(true);
        settingsGui.SetActive(false);
    }

    public void CloseGame()
    {
        Application.Quit();
    }
}
