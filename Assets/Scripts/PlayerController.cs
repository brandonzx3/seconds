using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public Vector3 checkpointPos;

    [Header("Components")]
    private Rigidbody2D rb;
    private Animator anim;
    private AudioSource source;
    private TrailRenderer trailRenderer;
    private CameraController cameraController;
    private Settings settings;

    [Header("Audio")]
    [SerializeField] private AudioClip jumpClip;

    [Header("Layer Masks")]
    [SerializeField] private LayerMask groundLayer;
    [SerializeField] private LayerMask wallLayer;
    [SerializeField] private LayerMask cornerCorrectLayer;

    [Header("Movement Variables")]
    [SerializeField] private float movementAcceleration = 70f;
    [SerializeField] private float maxMoveSpeed = 12f;
    [SerializeField] private float groundLinearDrag = 7f;
    private float horizontalDirection;
    private float verticalDirection;
    private bool changingDirection => (rb.velocity.x > 0f && horizontalDirection < 0f) || (rb.velocity.x < 0f && horizontalDirection > 0f);
    private bool facingRight = true;
    private bool canMove => !wallGrab;

    [Header("Jump Variables")]
    [SerializeField] private float jumpForce = 12f;
    [SerializeField] private float airLinearDrag = 2.5f;
    [SerializeField] private float fallMultiplier = 8f;
    [SerializeField] private float lowJumpFallMultiplier = 5f;
    [SerializeField] private float downMultiplier = 12f;
    [SerializeField] private int extraJumps = 1;
    [SerializeField] private float hangTime = .1f;
    [SerializeField] private float jumpBufferLength = .1f;
    private int extraJumpsValue;
    private float hangTimeCounter;
    private float jumpBufferCounter;
    private bool canJump => jumpBufferCounter > 0f && (hangTimeCounter > 0f || extraJumpsValue > 0 || onWall);
    private bool isJumping = false;

    [Header("Wall Movement Variables")]
    [SerializeField] private float wallSlideModifier = 0.5f;
    [SerializeField] private float wallRunModifier = 0.85f;
    [SerializeField] private float wallJumpXVelocityHaltDelay = 0.2f;
    private bool wallGrab => onWall && !onGround && Input.GetButton("WallGrab") && !wallRun;
    private bool wallSlide => onWall && !onGround && !Input.GetButton("WallGrab") && rb.velocity.y < 0f && !wallRun;
    private bool wallRun => onWall && verticalDirection > 0f;

    [Header("Dash Variables")]
    [SerializeField] private float dashSpeed = 15f;
    [SerializeField] private float dashLength = .3f;
    [SerializeField] private float dashBufferLength = .1f;
    [SerializeField] private float dashTrailThreshold = 15f;
    private float dashBufferCounter;
    public bool isDashing;
    private bool hasDashed;
    private bool shouldCancleDash;
    private bool canDash => dashBufferCounter > 0f && !hasDashed;

    [Header("Ground Collision Variables")]
    [SerializeField] private float groundRaycastLength;
    [SerializeField] private Vector3 groundRaycastOffset;
    private bool onGround;

    [Header("Wall Collision Variables")]
    [SerializeField] private float wallRaycastLength;
    private bool onWall;
    private bool onRightWall;

    [Header("Corner Correction Variables")]
    [SerializeField] private float topRaycastLength;
    [SerializeField] private Vector3 edgeRaycastOffset;
    [SerializeField] private Vector3 innerRaycastOffset;
    private bool canCornerCorrect;

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        source = GetComponent<AudioSource>();
        trailRenderer = GetComponent<TrailRenderer>();
        trailRenderer.emitting = false;
        checkpointPos = transform.position;
        cameraController = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraController>();
        settings = GameObject.FindGameObjectWithTag("GameController").GetComponent<Settings>();
    }

    private void Update()
    {
        horizontalDirection = GetInput().x;
        verticalDirection = GetInput().y;
        if (Input.GetButtonDown("Jump")) jumpBufferCounter = jumpBufferLength;
        else jumpBufferCounter -= Time.deltaTime;
        if (Input.GetButtonDown("Dash")) dashBufferCounter = dashBufferLength;
        else dashBufferCounter -= Time.deltaTime;
        Animation();
        if(!isDashing)
        {
            if (Mathf.Abs(rb.velocity.x) >= dashTrailThreshold || Mathf.Abs(rb.velocity.y) >= dashTrailThreshold)
            {
                trailRenderer.emitting = true;
            }
            else
            {
                trailRenderer.emitting = false;
            }
        }
    }

    private void FixedUpdate()
    {
        CheckCollisions();
        if (canDash) StartCoroutine(Dash(horizontalDirection, verticalDirection));
        if (!isDashing)
        {
            if (canMove) MoveCharacter();
            else rb.velocity = Vector2.Lerp(rb.velocity, (new Vector2(horizontalDirection * maxMoveSpeed, rb.velocity.y)), .5f * Time.deltaTime);
            if (onGround)
            {
                ApplyGroundLinearDrag();
                extraJumpsValue = extraJumps;
                hangTimeCounter = hangTime;
                hasDashed = false;
            }
            else
            {
                ApplyAirLinearDrag();
                FallMultiplier();
                hangTimeCounter -= Time.fixedDeltaTime;
                if (!onWall || rb.velocity.y < 0f || wallRun) isJumping = false;
            }
            if (canJump)
            {
                if (onWall && !onGround)
                {
                    if (!wallRun && (onRightWall && horizontalDirection > 0f || !onRightWall && horizontalDirection < 0f))
                    {
                        StartCoroutine(NeutralWallJump());
                    }
                    else
                    {
                        WallJump();
                    }
                    Flip();
                }
                else
                {
                    Jump(Vector2.up);
                }
            }
            if (!isJumping)
            {
                if (wallSlide) WallSlide();
                if (wallGrab) WallGrab();
                if (wallRun) WallRun();
                if (onWall) StickToWall();
            }
        }
        if (canCornerCorrect) CornerCorrect(rb.velocity.y);
    }

    private Vector2 GetInput()
    {
        return new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
    }

    private void MoveCharacter()
    {
        rb.AddForce(new Vector2(horizontalDirection, 0f) * movementAcceleration);

        if (Mathf.Abs(rb.velocity.x) > maxMoveSpeed)
            rb.velocity = new Vector2(Mathf.Sign(rb.velocity.x) * maxMoveSpeed, rb.velocity.y);
    }

    private void ApplyGroundLinearDrag()
    {
        if (Mathf.Abs(horizontalDirection) < 0.4f || changingDirection)
        {
            rb.drag = groundLinearDrag;
        }
        else
        {
            rb.drag = 0f;
        }
    }

    private void ApplyAirLinearDrag()
    {
        rb.drag = airLinearDrag;
    }

    private void Jump(Vector2 direction)
    {
        //source.PlayOneShot(jumpClip, 1f);
        if (!onGround && !onWall)
            extraJumpsValue--;

        ApplyAirLinearDrag();
        rb.velocity = new Vector2(rb.velocity.x, 0f);
        rb.AddForce(direction * jumpForce, ForceMode2D.Impulse);
        hangTimeCounter = 0f;
        jumpBufferCounter = 0f;
        isJumping = true;
    }

    private void WallJump()
    {
        Vector2 jumpDirection = onRightWall ? Vector2.left : Vector2.right;
        Jump(Vector2.up + jumpDirection);
    }

    IEnumerator NeutralWallJump()
    {
        Vector2 jumpDirection = onRightWall ? Vector2.left : Vector2.right;
        Jump(Vector2.up + jumpDirection);
        yield return new WaitForSeconds(wallJumpXVelocityHaltDelay);
        rb.velocity = new Vector2(0f, rb.velocity.y);
    }

    private void FallMultiplier()
    {
        if (verticalDirection < 0f)
        {
            rb.gravityScale = downMultiplier;
        }
        else
        {
            if (rb.velocity.y < 0)
            {
                rb.gravityScale = fallMultiplier;
            }
            else if (rb.velocity.y > 0 && !Input.GetButton("Jump"))
            {
                rb.gravityScale = lowJumpFallMultiplier;
            }
            else
            {
                rb.gravityScale = 1.8f;
            }
        }
    }

    void WallGrab()
    {
        rb.gravityScale = 0f;
        rb.velocity = Vector2.zero;
    }

    void WallSlide()
    {
        rb.velocity = new Vector2(rb.velocity.x, -maxMoveSpeed * wallSlideModifier);
    }

    void WallRun()
    {
        rb.velocity = new Vector2(rb.velocity.x, verticalDirection * maxMoveSpeed * wallRunModifier);
    }

    void StickToWall()
    {
        //Push player torwards wall
        if (onRightWall && horizontalDirection >= 0f)
        {
            rb.velocity = new Vector2(1f, rb.velocity.y);
        }
        else if (!onRightWall && horizontalDirection <= 0f)
        {
            rb.velocity = new Vector2(-1f, rb.velocity.y);
        }

        //Face correct direction
        if (onRightWall && !facingRight)
        {
            Flip();
        }
        else if (!onRightWall && facingRight)
        {
            Flip();
        }
    }

    void Flip()
    {
        facingRight = !facingRight;
        transform.Rotate(0f, 180f, 0f);
    }

    IEnumerator Dash(float x, float y)
    {
        float dashStartTime = Time.time;    
        hasDashed = true;
        isDashing = true;
        isJumping = false;
        trailRenderer.emitting = true;
        StartCoroutine(cameraController.CameraShake(0.1f, 0.1f * settings.GetScreenShake()));

        rb.velocity = Vector2.zero;
        rb.gravityScale = 0f;
        rb.drag = 0f;

        Vector2 dir;
        if (x != 0f || y != 0f) dir = new Vector2(x, y);
        else
        {
            if (facingRight) dir = new Vector2(1f, 0f);
            else dir = new Vector2(-1f, 0f);
        }

        while (Time.time < dashStartTime + dashLength && !shouldCancleDash)
        {
            rb.velocity = dir.normalized * dashSpeed;
            yield return null;
        }

        isDashing = false;
        
        if(shouldCancleDash)
        {
            rb.AddForce(-dir.normalized * 200);
        }

        shouldCancleDash = false;
        rb.velocity = new Vector2(rb.velocity.x, rb.velocity.y * 0.5f);
        yield return new WaitForSeconds(0.15f);
        trailRenderer.emitting = false;
    }

    void Animation()
    {
        if (isDashing)
        {
            anim.SetBool("isDashing", true);
            anim.SetBool("isGrounded", false);
            anim.SetBool("isFalling", false);
            anim.SetBool("isJumping", false);
            anim.SetFloat("horizontalDirection", 0f);
            anim.SetFloat("verticalDirection", 0f);
        }
        else
        {
            anim.SetBool("isDashing", false);

            if ((horizontalDirection < 0f && facingRight || horizontalDirection > 0f && !facingRight) && !wallGrab && !wallSlide)
            {
                Flip();
            }
            if (onGround)
            {
                anim.SetBool("isGrounded", true);
                anim.SetBool("isFalling", false);
                anim.SetFloat("horizontalDirection", Mathf.Abs(horizontalDirection));
            }
            else
            {
                anim.SetBool("isGrounded", false);
            }
            if (isJumping)
            {
                anim.SetBool("isJumping", true);
                anim.SetBool("isFalling", false);
                anim.SetFloat("verticalDirection", 0f);
            }
            else
            {
                anim.SetBool("isJumping", false);
                if (rb.velocity.y < 0f)
                {
                    anim.SetBool("isFalling", true);
                    anim.SetFloat("verticalDirection", 0f);
                }
            }
        }
    }

    void CornerCorrect(float Yvelocity)
    {
        //Push player to the right
        RaycastHit2D hit = Physics2D.Raycast(transform.position - innerRaycastOffset + Vector3.up * topRaycastLength, Vector3.left, topRaycastLength, cornerCorrectLayer);
        if (hit.collider != null)
        {
            float newPos = Vector3.Distance(new Vector3(hit.point.x, transform.position.y, 0f) + Vector3.up * topRaycastLength,
                transform.position - edgeRaycastOffset + Vector3.up * topRaycastLength);
            transform.position = new Vector3(transform.position.x + newPos, transform.position.y, transform.position.z);
            rb.velocity = new Vector2(rb.velocity.x, Yvelocity);
            return;
        }

        //Push player to the left
        hit = Physics2D.Raycast(transform.position + innerRaycastOffset + Vector3.up * topRaycastLength, Vector3.right, topRaycastLength, cornerCorrectLayer);
        if (hit.collider != null)
        {
            float newPos = Vector3.Distance(new Vector3(hit.point.x, transform.position.y, 0f) + Vector3.up * topRaycastLength,
                transform.position + edgeRaycastOffset + Vector3.up * topRaycastLength);
            transform.position = new Vector3(transform.position.x - newPos, transform.position.y, transform.position.z);
            rb.velocity = new Vector2(rb.velocity.x, Yvelocity);
        }
    }

    private void CheckCollisions()
    {
        //Ground Collisions
        onGround = Physics2D.Raycast(transform.position + groundRaycastOffset, Vector2.down, groundRaycastLength, groundLayer) ||
                    Physics2D.Raycast(transform.position - groundRaycastOffset, Vector2.down, groundRaycastLength, groundLayer);

        //Corner Collisions
        canCornerCorrect = Physics2D.Raycast(transform.position + edgeRaycastOffset, Vector2.up, topRaycastLength, cornerCorrectLayer) &&
                            !Physics2D.Raycast(transform.position + innerRaycastOffset, Vector2.up, topRaycastLength, cornerCorrectLayer) ||
                            Physics2D.Raycast(transform.position - edgeRaycastOffset, Vector2.up, topRaycastLength, cornerCorrectLayer) &&
                            !Physics2D.Raycast(transform.position - innerRaycastOffset, Vector2.up, topRaycastLength, cornerCorrectLayer);

        //Wall Collisions
        onWall = Physics2D.Raycast(transform.position, Vector2.right, wallRaycastLength, wallLayer) ||
                    Physics2D.Raycast(transform.position, Vector2.left, wallRaycastLength, wallLayer);
        onRightWall = Physics2D.Raycast(transform.position, Vector2.right, wallRaycastLength, wallLayer);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.layer == LayerMask.NameToLayer("Ground") && isDashing)
        {
            shouldCancleDash = true;
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;

        //Ground Check
        Gizmos.DrawLine(transform.position + groundRaycastOffset, transform.position + groundRaycastOffset + Vector3.down * groundRaycastLength);
        Gizmos.DrawLine(transform.position - groundRaycastOffset, transform.position - groundRaycastOffset + Vector3.down * groundRaycastLength);

        //Corner Check
        Gizmos.DrawLine(transform.position + edgeRaycastOffset, transform.position + edgeRaycastOffset + Vector3.up * topRaycastLength);
        Gizmos.DrawLine(transform.position - edgeRaycastOffset, transform.position - edgeRaycastOffset + Vector3.up * topRaycastLength);
        Gizmos.DrawLine(transform.position + innerRaycastOffset, transform.position + innerRaycastOffset + Vector3.up * topRaycastLength);
        Gizmos.DrawLine(transform.position - innerRaycastOffset, transform.position - innerRaycastOffset + Vector3.up * topRaycastLength);

        //Corner Distance Check
        Gizmos.DrawLine(transform.position - innerRaycastOffset + Vector3.up * topRaycastLength,
                        transform.position - innerRaycastOffset + Vector3.up * topRaycastLength + Vector3.left * topRaycastLength);
        Gizmos.DrawLine(transform.position + innerRaycastOffset + Vector3.up * topRaycastLength,
                        transform.position + innerRaycastOffset + Vector3.up * topRaycastLength + Vector3.right * topRaycastLength);

        //Wall Check
        Gizmos.DrawLine(transform.position, transform.position + Vector3.right * wallRaycastLength);
        Gizmos.DrawLine(transform.position, transform.position + Vector3.left * wallRaycastLength);
    }
}
