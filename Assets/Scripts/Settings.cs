using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Settings : MonoBehaviour
{
    [SerializeField] private Slider masterVolumeSlider;
    [SerializeField] private Slider musicVolumeSlider;
    [SerializeField] private Slider sfxVolumeSlider;
    [SerializeField] private Slider screenShakeSlider;

    private float masterVolume;
    private float musicVolume;
    private float sfxVolume;
    private float screenShake;

    // Start is called before the first frame update
    void Start()
    {
        LoadSettings();
    }

    public void SaveSettings()
    {
        UpdateSettings();
        PlayerPrefs.SetFloat("masterVolume", masterVolume);
        PlayerPrefs.SetFloat("musicVolume", musicVolume);
        PlayerPrefs.SetFloat("sfxVolume", sfxVolume);
        PlayerPrefs.SetFloat("screenShake", screenShake);
        PlayerPrefs.Save();
    }

    public void LoadSettings()
    {
        masterVolume = PlayerPrefs.GetFloat("masterVolume", 1);
        musicVolume = PlayerPrefs.GetFloat("musicVolume", 1);
        sfxVolume = PlayerPrefs.GetFloat("sfxVolume", 1);
        screenShake = PlayerPrefs.GetFloat("screenShake", 1);

        masterVolumeSlider.value = masterVolume;
        musicVolumeSlider.value = musicVolume;
        sfxVolumeSlider.value = sfxVolume;
        screenShakeSlider.value = screenShake;
    }

    private void UpdateSettings()
    {
        masterVolume = masterVolumeSlider.value;
        musicVolume = musicVolumeSlider.value;
        sfxVolume = sfxVolumeSlider.value;
        screenShake = screenShakeSlider.value;
    }


    public float GetMasterVolume()
    {
        return masterVolume;
    }

    public float GetMusicVolume()
    {
        return musicVolume;
    }

    public float GetSfxVolume()
    {
        return sfxVolume;
    }

    public float GetScreenShake()
    {
        return screenShake;
    }
}
