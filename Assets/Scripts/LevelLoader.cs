using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelLoader : MonoBehaviour
{
    //public Animator anim;

    public void LoadScene(int scene)
    {
        StartCoroutine(LoadLevel(scene));
    }

    public void LoadScene(string scene)
    {
        StartCoroutine(LoadLevel(scene));
    }

    IEnumerator LoadLevel(int scene)
    {
        //anim.SetTrigger("Start");
        yield return new WaitForSecondsRealtime(1);
        //anim.SetBool("PlayEndTransition", true);
        Time.timeScale = 1.0f;
        SceneManager.LoadScene(scene);
    }

    IEnumerator LoadLevel(string scene)
    {
        //anim.SetTrigger("Start");
        yield return new WaitForSecondsRealtime(1);
        //anim.SetBool("PlayEndTransition", true);
        Time.timeScale = 1.0f;
        SceneManager.LoadScene(scene);
    }
}
