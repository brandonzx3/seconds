using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public Vector3 offset;
    public float smoothSpeed;
    Vector3 desiredPosition;
    public GameObject target;
    public GameObject actualCamera;

    void Start()
    {
        target = GameObject.FindGameObjectWithTag("Player");
    }

    
    void FixedUpdate()
    {
        desiredPosition = target.transform.position + offset;
        transform.position = Vector3.Lerp(transform.position, desiredPosition, smoothSpeed);
    }

    public IEnumerator CameraShake(float duration, float magnitude)
    {
        float elapsed = 0.0f;
        while (elapsed < duration)
        {
            float x = Random.Range(-1f, 1f) * magnitude;
            float y = Random.Range(-1f, 1f) * magnitude;
            
            actualCamera.transform.localPosition = new Vector3(x, y, actualCamera.transform.localPosition.z);
            elapsed += Time.deltaTime;
            yield return null;
        }
        actualCamera.transform.localPosition = new Vector3(0, 0, 0);
    }
}
