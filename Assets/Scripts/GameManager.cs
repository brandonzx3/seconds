using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{

    public bool gameFirstOpen = true;
    GameObject player;

    void Start()
    {
        if (GameObject.FindGameObjectsWithTag("GameController").Length > 1)
        {
            GameObject.Destroy(gameObject);
        }
        DontDestroyOnLoad(this);

        if(gameFirstOpen)
        {
            //levelLoader.anim.SetBool("PlayEndTransition", false);
            gameFirstOpen = false;
        }
    }
    public void CloseGame()
    {
        Application.Quit();
    }

    public void KillPlayer()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        player.SetActive(false);
        StartCoroutine(RespawnPlayer());
    }

    IEnumerator RespawnPlayer()
    {
        yield return new WaitForSeconds(2);
        player.SetActive(true);
        PlayerController controller = player.GetComponent<PlayerController>();
        player.transform.position = controller.checkpointPos;
        player.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
        controller.isDashing = false;
    }
}
